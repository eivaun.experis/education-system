# Education system
A simple CLI education system.

## Overview
The program can add and remove students, teachers, and courses. As well as add and remove a teacher/student from a course.
One can list all the students, teachers, and courses in addition to get more detailed information on a single entry.
The program automatically saves the data to ```./edu.bin``` at shutdown and loads the data at startup. The user can also manually issue a save/load command.

## Dependencies
### CMake
The project uses the CMake build system. For install instructions see https://cmake.org/

### HPS
HPS is used for serialization. This library is included in the repo.

## Build
The project is built using CMake with the provided ```CMakeLists.txt```

### VS Code
This requires the CMake extension for VS Code.\
Configure: ```Cmake: Configure```\
Compile: ```CMake: Build```

### Terminal
Configure: ```cmake -B build```\
Compile: ```make -C build```

## Run
```
$ ./build/edu
```

## Contributors
Eivind Vold Aunebakk
