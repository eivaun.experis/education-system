#pragma once
#include <string>
#include <fstream>

class File
{
  public:
    template<class T>
    inline static void save_to_file(const T& value, const std::string& file_path)
    {
        if (auto file_stream = std::ofstream(file_path, std::ios::binary))
        {
            hps::to_stream(value, file_stream);
            std::cout << "Saved to file: " << file_path << std::endl;
        }
        else
        {
            std::cout << "Failed to write to file: " << file_path << std::endl;
        }
    }

    template<class T>
    inline static void load_from_file(T& value, const std::string& file_path)
    {
        if (auto file_stream = std::ifstream(file_path, std::ios::binary))
        {
            value = hps::from_stream<T>(file_stream);
            std::cout << "Loaded from file: " << file_path << std::endl;
        }
        else
        {
            std::cout << "Failed to open file: " << file_path << std::endl;
        }
    }
};
