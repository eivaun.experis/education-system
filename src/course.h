#pragma once

#include <string>
#include <set>
#include "common.h"
#include "student.h"
#include "teacher.h"
#include "printer.h"
#include "input.h"

class Course
{
    inline static Id next_id = 0;
    Id id;

    std::string name;
    int code;

    std::set<Id> teachers;
    std::set<Id> students;

  public:
    Course() = default;
    Course(const std::string& name, int code) : id(next_id), name(name), code(code) {};

    Id get_id() const { return id; }
    int get_code() const { return code; }
    const std::string& get_name() const { return name; }
    const std::set<Id>& get_teachers() const { return teachers; }
    const std::set<Id>& get_students() const { return students; }

    // Save with HPS
    template<class B>
    void serialize(B& buf) const
    {
        buf << id << name << code << teachers << students;
    }
    // Load with HPS
    template<class B>
    void parse(B& buf)
    {
        buf >> id >> name >> code >> teachers >> students;
    }

    // Adds the id of a student/teacher to the students/teachers set
    template<class T>
    bool add(Id person_id)
    {
        if (get_id_set<T>().count(person_id)) return false;

        get_id_set<T>().insert(person_id);
        return true;
    }

    // Removes the id of a student/teacher from the students/teachers set
    template<class T>
    bool remove(Id person_id)
    {
        if (!get_id_set<T>().count(person_id)) return false;

        get_id_set<T>().erase(person_id);
        return true;
    }

  private:
    // Returns either the students or the teachers set based on the type of T
    template<class T>
    std::set<Id>& get_id_set()
    {
        if constexpr (std::is_same<T, Teacher>::value)
            return teachers;
        else if constexpr (std::is_same<T, Student>::value)
            return students;
        else
            static_assert(always_false<T>, "Unsupported type T");
    }
};

// Add support for Course in Input
template<>
Course Input::read();

// Add support for Course in printer
template<>
void Printer::print(const School& school, const Course& course);

template<>
void Printer::print_summary(const Course& course);
