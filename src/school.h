#pragma once

#include <string>
#include <map>

#include "common.h"
#include "student.h"
#include "teacher.h"
#include "course.h"

class School
{
    std::map<Id, Student> students;
    std::map<Id, Teacher> teachers;
    std::map<Id, Course> courses;

  public:
    const std::map<Id, Student>& get_students() const { return students; }
    const std::map<Id, Teacher>& get_teachers() const { return teachers; }
    const std::map<Id, Course>& get_courses() const { return courses; }

    // Check if a student/teacher/course exists
    template<class T>
    bool has(Id id) const
    {
        return get_map<T>().count(id);
    }

    // Add a student/teacher/course
    template<class T>
    void add(T& val)
    {
        get_map<T>()[val.get_id()] = val;
    }

    // Remove a student/teacher/course
    template<class T>
    bool remove(Id id)
    {
        if (has<T>(id))
        {
            get_map<T>().erase(id);
            return true;
        }
        return false;
    }

    // Get a student/teacher/course by id
    template<class T>
    const T* get(Id id) const
    {
        if (has<T>(id))
        {
            return &get_map<T>().at(id);
        }
        return nullptr;
    }

    // Add a student/teacher to a course
    template<class T>
    bool add_to_course(Id person_id, Id course_id)
    {
        if (!has<T>(person_id) || !has<Course>(course_id)) return false;
        return courses.at(course_id).add<T>(person_id);
    }

    // Remove a student/teacher from a course
    template<class T>
    bool remove_from_course(Id person_id, Id course_id)
    {
        if (!has<T>(person_id) || !has<Course>(course_id)) return false;
        return courses.at(course_id).remove<T>(person_id);
    }

    // Save with HPS
    template<class B>
    void serialize(B& buf) const
    {
        buf << students << teachers << courses;
    }

    // Load with HPS
    template<class B>
    void parse(B& buf)
    {
        buf >> students >> teachers >> courses;
    }

    // Returns the students/teachers/courses map based on the type of T
    template<class T>
    const std::map<Id, T>& get_map() const
    {
        if constexpr (std::is_same<T, Teacher>::value)
            return teachers;
        else if constexpr (std::is_same<T, Student>::value)
            return students;
        else if constexpr (std::is_same<T, Course>::value)
            return courses;
        else
            static_assert(always_false<T>, "Unsupported type T");
    }

  private:
    // Returns the students/teachers/courses map based on the type of T
    template<class T>
    std::map<Id, T>& get_map()
    {
        if constexpr (std::is_same<T, Teacher>::value)
            return teachers;
        else if constexpr (std::is_same<T, Student>::value)
            return students;
        else if constexpr (std::is_same<T, Course>::value)
            return courses;
        else
            static_assert(always_false<T>, "Unsupported type T");
    }
};
