#pragma once

#include <iostream>

class School;

// Used to print items related to a School
class Printer
{
    inline static std::ostream* s_out = &std::cout;

  public:
    inline static std::ostream& out() { return *s_out; }

    // Set the output stream
    inline static void set_out(std::ostream& new_out) { s_out = &new_out; }

    template<class T>
    static void print(const School& school, const T& value);

    template<class T>
    static void print_summary(const T& value);
};
