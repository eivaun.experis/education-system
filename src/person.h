#pragma once

#include <string>
#include "common.h"

class Person
{
  protected:
    std::string first_name, last_name;

  public:
    Person() = default;
    Person(const std::string& first_name, const std::string& last_name) : first_name(first_name), last_name(last_name) {}

    const std::string& get_first_name() const { return first_name; }
    const std::string& get_last_name() const { return last_name; }

    template<class B>
    void serialize(B& buf) const
    {
        buf << first_name << last_name;
    }

    template<class B>
    void parse(B& buf)
    {
        buf >> first_name >> last_name;
    }
};
