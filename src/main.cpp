#include <iostream>
#include <string>
#include "hps/hps.h"

#include "input.h"
#include "school.h"
#include "file.h"
#include "menu.h"

int main()
{
    School school;
    std::string file_name = "edu.bin";

    // Load at startup
    File::load_from_file(school, file_name);

    Menu student_menu = {
      {"List students",
       [&]()
       {
           for (const auto& [id, val] : school.get_students())
               Printer::print_summary(val);
       }},
      {"Add student",
       [&]()
       {
           auto val = Input::read<Student>();
           school.add(val);
           Printer::print_summary(val);
       }},
      {"Remove student",
       [&]()
       {
           Id id = Input::read_int("Student id");
           if (school.remove<Student>(id))
               std::cout << "Removed student\n";
           else
               std::cout << "Failed to remove student\n";
       }},
      {"Display student",
       [&]()
       {
           Id id    = Input::read_int("Student id");
           auto ptr = school.get<Student>(id);
           if (ptr)
               Printer::print(school, *ptr);
           else
               std::cout << "No student with id " << id << "\n";
       }},
    };

    Menu teacher_menu = {
      {"List teacher",
       [&]()
       {
           for (const auto& [id, val] : school.get_teachers())
               Printer::print_summary(val);
       }},
      {"Add teacher",
       [&]()
       {
           auto val = Input::read<Teacher>();
           school.add(val);
           Printer::print_summary(val);
       }},
      {"Remove student",
       [&]()
       {
           Id id = Input::read_int("Teacher id");
           if (school.remove<Teacher>(id))
               std::cout << "Removed teacher\n";
           else
               std::cout << "Failed to remove teacher\n";
       }},
      {"Display teacher",
       [&]()
       {
           Id id    = Input::read_int("Teacher id");
           auto ptr = school.get<Teacher>(id);
           if (ptr)
               Printer::print(school, *ptr);
           else
               std::cout << "No teacher with id " << id << "\n";
       }},
    };

    Menu course_menu = {
      {"List courses",
       [&]()
       {
           for (const auto& [id, val] : school.get_courses())
               Printer::print_summary(val);
       }},
      {"Add course",
       [&]()
       {
           auto val = Input::read<Course>();
           school.add(val);
           Printer::print_summary(val);
       }},
      {"Remove course",
       [&]()
       {
           Id id = Input::read_int("Course id");
           if (school.remove<Course>(id))
               std::cout << "Removed course\n";
           else
               std::cout << "Failed to remove course\n";
       }},
      {"Display course",
       [&]()
       {
           Id id    = Input::read_int("Course id");
           auto ptr = school.get<Course>(id);
           if (ptr)
               Printer::print(school, *ptr);
           else
               std::cout << "No course with id " << id << "\n";
       }},
      {"Add student to course",
       [&]()
       {
           Id sid = Input::read_int("Student id");
           Id cid = Input::read_int("Course id");
           if (school.add_to_course<Student>(sid, cid))
               std::cout << "Successfully added the student to the course\n";
           else
               std::cout << "Failed to add the student to the course\n";
       }},
      {"Remove student from course",
       [&]()
       {
           Id sid = Input::read_int("Student id");
           Id cid = Input::read_int("Course id");
           if (school.remove_from_course<Student>(sid, cid))
               std::cout << "Successfully removed the student from the course\n";
           else
               std::cout << "Failed to remove the student from the course\n";
       }},
      {"Add teacher to course",
       [&]()
       {
           Id tid = Input::read_int("Teacher id");
           Id cid = Input::read_int("Course id");
           if (school.add_to_course<Teacher>(tid, cid))
               std::cout << "Successfully added the teacher to the course\n";
           else
               std::cout << "Failed to add the teacher to the course\n";
       }},
      {"Remove teacher from course",
       [&]()
       {
           Id tid = Input::read_int("Teacher id");
           Id cid = Input::read_int("Course id");
           if (school.remove_from_course<Teacher>(tid, cid))
               std::cout << "Successfully removed the teacher from the course\n";
           else
               std::cout << "Failed to remove the teacher from the course\n";
       }},
    };

    // Display the main menu
    do_menu("Main Menu",
            {
              {"Students", [&]() { do_menu("Students", student_menu); }},
              {"Teachers", [&]() { do_menu("Teachers", teacher_menu); }},
              {"Courses", [&]() { do_menu("Courses", course_menu); }},
              {"Save", [&]() { File::save_to_file(school, file_name); }},
              {"Load", [&]() { File::load_from_file(school, file_name); }},
            });

    // Save at shutdown
    File::save_to_file(school, file_name);
}
