#pragma once

#include "person.h"
#include "printer.h"
#include "input.h"

class Teacher : public Person
{
    inline static Id next_id = 0;
    Id id;

    std::string department;

  public:
    Teacher() = default;
    Teacher(const std::string& first_name, const std::string& last_name, const std::string& department) :
      Person(first_name, last_name), id(next_id++), department(department) {};

    Id get_id() const { return id; }
    const std::string& get_department() const { return department; }

    // Save with HPS
    template<class B>
    void serialize(B& buf) const
    {
        Person::serialize(buf);
        buf << id << department;
    }

    // Load with HPS
    template<class B>
    void parse(B& buf)
    {
        Person::parse(buf);
        buf >> id >> department;
    }
};

// Add support for Teacher in Input
template<>
Teacher Input::read();

// Add support for Teacher in printer
template<>
void Printer::print(const School& school, const Teacher& teacher);

template<>
void Printer::print_summary(const Teacher& teacher);
