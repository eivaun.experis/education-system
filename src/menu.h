#pragma once

#include <string>
#include <functional>
#include <vector>

// A menu item
struct MenuItem
{
    // The name of the item
    std::string name;
    // The function executed if the item is selected
    std::function<void()> func;
};
// A menu is just a collection of items
using Menu = std::vector<MenuItem>;

// Display a menu with the given items
inline void do_menu(const std::string& title, Menu menu)
{
    bool running   = true;
    bool first_run = true;
    while (running)
    {
        if (!first_run) std::cout << "==== " << title << " ===\n";
        first_run = false;
        for (size_t i = 0; i < menu.size(); i++)
        {
            std::cout << "(" << i << ") " << menu[i].name << std::endl;
        }
        std::cout << "(" << menu.size() << ") Back" << std::endl;
        size_t input = Input::read_ulong("Enter command: ");
        std::cout << std::endl;

        if (input < menu.size())
        {
            std::cout << "==== " << menu[input].name << " ====\n";
            menu[input].func();
            std::cout << std::endl;
        }
        else if (input == menu.size())
        {
            break;
        }
        else
        {
            std::cout << "Unknown command: " << input << std::endl << std::endl;
            continue;
        }
    }
}
