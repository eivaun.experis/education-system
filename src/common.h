#pragma once

// To have a more explicit id type without actually creating a class
using Id = int;

// Used in place of 'false' static_assert as static_assert(false) causes the code to be ill formed regardless of whether it's called or not
template<class... T>
constexpr bool always_false = false;
