#pragma once

#include <iostream>

// Input class used to read values from a stream
class Input
{
    inline static std::istream* s_in  = &std::cin;
    inline static std::ostream* s_out = &std::cout;

  public:
    inline static std::istream& in() { return *s_in; }
    inline static std::ostream& out() { return *s_out; }

    // Set the input stream
    inline static void set_in(std::istream& new_in) { s_in = &new_in; }

    // Set the output stream
    inline static void set_out(std::ostream& new_out) { s_out = &new_out; }

    // Allows reading custom objects
    template<class T>
    static T read();

    inline static int read_int(const std::string& prompt)
    {
        while (true)
        {
            out() << prompt << ": ";
            std::string line;
            std::getline(in(), line);
            try
            {
                return std::stoi(line);
            }
            catch (const std::invalid_argument& e)
            {
            }
        }
    }

    inline static unsigned long read_ulong(const std::string& prompt)
    {
        while (true)
        {
            out() << prompt << ": ";
            std::string line;
            std::getline(in(), line);
            try
            {
                return std::stoul(line);
            }
            catch (const std::invalid_argument& e)
            {
            }
        }
    }

    inline static std::string read_string(const std::string& prompt, bool allow_empty = false)
    {
        while (true)
        {
            out() << prompt << ": ";
            std::string line;
            std::getline(in(), line);
            if (line.length() > 0 || allow_empty) return line;
        }
    }
};
