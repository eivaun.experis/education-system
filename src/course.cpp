#include "course.h"
#include "school.h"

template<>
Course Input::read()
{
    std::string name = read_string("Name");
    int code         = read_int("Code");
    return Course(name, code);
}

template<>
void Printer::print(const School& school, const Course& course)
{
    out() << "Name: " << course.get_name() << "\n";
    out() << "Code: " << course.get_code() << "\n";

    out() << "Teachers:\n";
    auto teachers    = school.get_map<Teacher>();
    auto teacher_ids = course.get_teachers();
    if (teacher_ids.size() == 0)
    {
        out() << "No teachers assigned to this course\n";
    }
    else
    {
        for (const auto& id : teacher_ids)
            Printer::print_summary(teachers.at(id));
    }

    out() << "Students:\n";
    auto students    = school.get_map<Student>();
    auto student_ids = course.get_students();
    if (student_ids.size() == 0)
    {
        out() << "No students enrolled to this course\n";
    }
    else
    {
        for (const auto& id : student_ids)
            Printer::print_summary(students.at(id));
    }
};

template<>
void Printer::print_summary(const Course& course)
{
    out() << "(" << course.get_id() << ") " << course.get_code() << " " << course.get_name() << "\n";
};
