#include "teacher.h"
#include "school.h"

template<>
Teacher Input::read()
{
    std::string first      = read_string("First name");
    std::string last       = read_string("Last name");
    std::string department = read_string("Department");
    return Teacher(first, last, department);
}

template<>
void Printer::print(const School& school, const Teacher& teacher)
{
    out() << "First name: " << teacher.get_first_name() << "\n Last name: " << teacher.get_last_name() << "\n Department: " << teacher.get_department() << "\nCourses:\n";

    for (const auto& [c_id, c] : school.get_map<Course>())
    {
        if (c.get_students().count(teacher.get_id())) Printer::print_summary(c);
    }
};

template<>
void Printer::print_summary(const Teacher& teacher)
{
    out() << "(" << teacher.get_id() << ") " << teacher.get_first_name() << " " << teacher.get_last_name() << "\n";
};
