#pragma once

#include "person.h"
#include "printer.h"
#include "input.h"

class Student : public Person
{
    inline static Id next_id = 0;
    Id id;

  public:
    Student() = default;
    Student(const std::string& first_name, const std::string& last_name) : Person(first_name, last_name), id(next_id++) {};

    Id get_id() const { return id; }

    // Save with HPS
    template<class B>
    void serialize(B& buf) const
    {
        Person::serialize(buf);
        buf << id;
    }

    // Load with HPS
    template<class B>
    void parse(B& buf)
    {
        Person::parse(buf);
        buf >> id;
    }
};

// Add support for Student in Input
template<>
Student Input::read();

// Add support for Student in printer
template<>
void Printer::print(const School& school, const Student& student);

template<>
void Printer::print_summary(const Student& student);
