#include "student.h"
#include "school.h"

template<>
Student Input::read()
{
    std::string first = read_string("First name");
    std::string last  = read_string("Last name");
    return Student(first, last);
}

template<>
void Printer::print(const School& school, const Student& student)
{
    out() << "First name: " << student.get_first_name() << "\n Last name: " << student.get_last_name() << "\nCourses:" << std::endl;
    for (const auto& [c_id, c] : school.get_map<Course>())
    {
        if (c.get_students().count(student.get_id())) Printer::print_summary(c);
    }
};

template<>
void Printer::print_summary(const Student& student)
{
    out() << "(" << student.get_id() << ") " << student.get_first_name() << " " << student.get_last_name() << std::endl;
};
